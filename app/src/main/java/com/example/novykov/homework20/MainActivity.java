package com.example.novykov.homework20;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.novykov.homework20.adapter.FragmentsAdapter;
import com.example.novykov.homework20.fragments.AboutFragment;
import com.example.novykov.homework20.fragments.FriendsFragment;
import com.example.novykov.homework20.fragments.PhotosFragment;
import com.example.novykov.homework20.fragments.SettingsFragment;
import com.viewpagerindicator.LinePageIndicator;

import static android.graphics.Color.rgb;

public class MainActivity extends AppCompatActivity {

    protected TextView previousTextView;
    protected TextView nextTextView;
    protected ViewPager viewPager;
    protected LinePageIndicator linePageIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        previousTextView = findViewById(R.id.tv_previous);
        nextTextView = findViewById(R.id.tv_next);

        viewPager = findViewById(R.id.viewPager);
        initViewPagerAdapter();
        linePageIndicator = findViewById(R.id.indicator);
        linePageIndicator.setCentered(true);
        linePageIndicator.offsetTopAndBottom(10);
        linePageIndicator.setSelectedColor(rgb(237, 14, 237));
        linePageIndicator.setViewPager(viewPager);
        initTextViewVisibility();

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int state) {
                initTextViewVisibility();
            }
        });

        nextTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            }
        });
        previousTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
            }
        });

    }

    private void initViewPagerAdapter() {

        FragmentsAdapter adapter = new FragmentsAdapter(getSupportFragmentManager());

        FriendsFragment friendsFragment = FriendsFragment.newInstance();
        adapter.addFragment(friendsFragment);

        PhotosFragment photosFragment = PhotosFragment.newInstance();
        adapter.addFragment(photosFragment);

        SettingsFragment settingsFragment = SettingsFragment.newInstance();
        adapter.addFragment(settingsFragment);

        AboutFragment aboutFragment = AboutFragment.newInstance();
        adapter.addFragment(aboutFragment);

        this.viewPager.setAdapter(adapter);
    }

    private void initTextViewVisibility() {
        if (viewPager.getCurrentItem() == 0) {
            previousTextView.setVisibility(View.INVISIBLE);
            nextTextView.setVisibility(View.VISIBLE);
        }
        if (viewPager.getCurrentItem() == 1) {
            previousTextView.setVisibility(View.VISIBLE);
            nextTextView.setVisibility(View.VISIBLE);
        }
        if (viewPager.getCurrentItem() == 2) {
            previousTextView.setVisibility(View.VISIBLE);
            nextTextView.setVisibility(View.VISIBLE);
        }
        if (viewPager.getCurrentItem() == 3) {
            previousTextView.setVisibility(View.VISIBLE);
            nextTextView.setVisibility(View.INVISIBLE);
        }

    }
}
