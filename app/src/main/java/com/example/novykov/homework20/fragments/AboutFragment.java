package com.example.novykov.homework20.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.novykov.homework20.R;

public class AboutFragment extends Fragment {
    protected ImageView imageAboutView;
    protected TextView textAboutView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_about, container, false);
        imageAboutView = fragmentView.findViewById(R.id.iv_about);
        textAboutView = fragmentView.findViewById(R.id.tv_about);
        imageAboutView.setImageResource(R.drawable.about);
        return fragmentView;
    }

    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }
}