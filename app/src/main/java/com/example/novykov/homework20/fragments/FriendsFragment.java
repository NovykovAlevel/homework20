package com.example.novykov.homework20.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.novykov.homework20.R;

public class FriendsFragment extends Fragment {

    protected ImageView imageFriendsView;
    protected TextView textFriendsView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView =  inflater.inflate(R.layout.fragment_friends, container, false);
        imageFriendsView = fragmentView.findViewById(R.id.iv_friends);
        textFriendsView = fragmentView.findViewById(R.id.tv_friends);
        imageFriendsView.setImageResource(R.drawable.friends);
        return fragmentView;
    }

    public static FriendsFragment newInstance() {
        FriendsFragment fragment = new FriendsFragment();
        return fragment;
    }
}
