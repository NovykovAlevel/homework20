package com.example.novykov.homework20.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.novykov.homework20.R;

public class SettingsFragment extends Fragment {
    protected ImageView imageSettingsView;
    protected TextView textSettingsView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView =  inflater.inflate(R.layout.fragment_settings, container, false);
        imageSettingsView = fragmentView.findViewById(R.id.iv_settings);
        textSettingsView = fragmentView.findViewById(R.id.tv_settings);
        imageSettingsView.setImageResource(R.drawable.settings);
        return fragmentView;
    }
    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }
}
