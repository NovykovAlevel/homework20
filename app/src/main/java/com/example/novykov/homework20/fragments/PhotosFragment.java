package com.example.novykov.homework20.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.novykov.homework20.R;

public class PhotosFragment extends Fragment {
    protected ImageView imagePhotosView;
    protected TextView textPhotosView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView =  inflater.inflate(R.layout.fragment_photos, container, false);
        imagePhotosView = fragmentView.findViewById(R.id.iv_photos);
        textPhotosView = fragmentView.findViewById(R.id.tv_photos);
        imagePhotosView.setImageResource(R.drawable.photos);
        return fragmentView;
    }

    public static PhotosFragment newInstance() {
        PhotosFragment fragment = new PhotosFragment();
        return fragment;
    }
}
